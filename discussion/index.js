//[OBJECTIVE] Create a server-side app using Express Web Framework

//Relate this task to something that you do on a daily basis.

//[SECTION]apppend the entire app to our node package manager.

	//package.json -> the "heart" of every node project this also contains different metadata that describes the structure of the project.

	//scripts -> is used to declare and describe custom commands and keyword that can be used to execute this project with the correct runtime environment.

	//NOTE: "start" is globally recognized amongst node projects and frameworks as the 'default' command script to excute a task/project
	//however for *uncoventional* keywords or command you have to append the command "run"
	//syntax: 

//1. Identify and prepare the ingredients.
const express = require("express");

//express => will be used as the main component to create the server.
//we need to be able to gather/acquire the utilities and components needed that the express libaray will provide us.
	//=> require() -> directive used to get the library/component needed inside the module

	//prepare the environment in which the project will be served

//[SECTION] Preparing a Remote repository for our Node project

  //Note: Always DISABLE the node_modules folder
  //WHY? 
  		//->it will take up to much space in our repository making a lot more difficult to stage upon committing the changes in our remote repo.
  		//-> if ever that you will deploy your node project on deployment platforms (heroku, netlify, vercel) the project will automatically be rejected because node_modules is not recognized on various deploment platforms.
  //HOW? using a .gitignore module
  			

//[SECTION]Create a Runtime environment that automatically autofix all the chages in our app.

	//were going to use utility called nodemon.
	//upon starting the entry point module with nodemon you will be able to the 'append' the application with the proper run time environmant. allowing you to save time and effort upon commiting changes.

//you can even insert items like text art into your run time environment
console.log(`
	Welcome to our Express API Server
	──────────────────▒
─────────────────░█
────────────────███
───────────────██ღ█
──────────────██ღ▒█──────▒█
─────────────██ღ░▒█───────██
─────────────█ღ░░ღ█──────█ღ▒█
────────────█▒ღ░▒ღ░█───██░ღღ█
───────────░█ღ▒░░▒ღ░████ღღღ█
───░───────█▒ღ▒░░░▒ღღღ░ღღღ██─────░█
───▓█─────░█ღ▒░░░░░░░▒░ღღ██─────▓█░
───██─────█▒ღ░░░░░░░░░░ღ█────▓▓██
───██────██ღ▒░░░░░░░░░ღ██─░██ღ▒█
──██ღ█──██ღ░▒░░░░░░░░░░ღ▓██▒ღღ█
──█ღღ▓██▓ღ░░░▒░░░░░░░░▒░ღღღ░░▓█
─██ღ▒▒ღღ░░ღღღღ░░▒░░░░ ღღღღ░░ღღღ██
─█ღ▒ღღ█████████ღღ▒░ღ██████████ღ▒█░
██ღღ▒████████████ღღ████████████░ღ█▒
█░ღღ████████████████████████████ღღ█
█▒ღ██████████████████████████████ღ█
██ღღ████████████████████████████ღ██
─██ღღ██████████████████████████ღ██
──░██ღღ██████████████████████ღღ██
────▓██ღ▒██████████████████▒ღ██
───░──░███ღ▒████████████▒ღ███
────░░───▒██ღღ████████▒ღ██
───────────▒██ღ██████ღ██
─────────────██ღ████ღ█
───────────────█ღ██ღ█
────────────────█ღღ█
────────────────█ღ█░
─────────────────██░
`)
